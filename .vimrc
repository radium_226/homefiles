colorscheme wombat
set number
filetype plugin indent on
let fortran_do_enddo=1
set smartindent
set autoindent
set expandtab
set tabstop=3
set shiftwidth=3
syntax on

" Настраиваем переключение раскладок клавиатуры по <C-^>
"set keymap=russian-jcukenwin
"
" " Раскладка по умолчанию - английская
"set iminsert=0

"set encoding=utf-8                                  " set charset translation encoding
"set termencoding=utf-8                              " set terminal encoding
"set fileencoding=utf-8                              " set save encoding
set fileencodings=utf8,cp1251   " список предполагаемых кодировок, в порядке предпочтения
set clipboard=unnamedplus
