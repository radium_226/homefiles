---------------------------------------------------
-- Licensed under the GNU General Public License
--  * (c) 2011, sledopit <jabber sledopit@jabber.ru>
---------------------------------------------------

-- {{{ Grab environment
local type = type
local tonumber = tonumber
local io = { popen = io.popen }
local setmetatable = setmetatable
local string = { match = string.match }
-- }}}

-- Yandex: provides count of new e-mail on Yandex
module("vicious.yandex")

-- {{{ Yandex widget type
local function worker(format, warg)
    local mail = {
        ["{count}"]   = 0,
    ["{color}"] = "#aaaaaa"
    }
-- Here you need to enter your token and login
-- for details about yandex API look at http://pdd.yandex.ru/help/section72/
local token = "вставить_ваш_токен_сюда"
local login = "вставить_ваш_логин_сюда"
    -- Get info from the Yandex
    local f = io.popen("wget --no-check-certificate -qO - \"https://pddimp.yandex.ru/get_mail_info.xml?token="..token.."&login="..login.."\"")
    for line in f:lines() do
        mail["{count}"] = --some
        tonumber(string.match(line, "<ok new_messages=\"([%d]+)\"/>")) or mail["{count}"]
           if mail["{count}"] > 0 then
            mail["{color}"] = "red"
           end
    end
    f:close()
    return mail
end
-- }}}

setmetatable(_M, { __call = function(_, ...) return worker(...) end })
